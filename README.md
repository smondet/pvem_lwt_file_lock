`Pvem_lwt_file_lock`
====================

This library provides a single module `File_lock` (c.f. module type
`FILE_LOCK`), it uses `Pvem_lwt_unix` to access the file-system.

You may run the tests:

    ./test/file_lock_test.ml
