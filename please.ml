#! /usr/bin/env ocaml
 open Printf
 module List = ListLabels
 let say fmt = ksprintf (printf "Please-> %s\n%!") fmt
 let cmdf fmt =
   ksprintf (fun s -> ignore (Sys.command s)) fmt
 let chain l =
   List.iter l ~f:(fun cmd ->
       printf "! %s\n%!" cmd;
       if  Sys.command cmd = 0
       then ()
       else ksprintf failwith "%S failed." cmd
     )
 let args = Array.to_list Sys.argv
 let in_build_directory f =
   cmdf "mkdir -p _build/";
   Sys.chdir "_build/";
   begin try
     f ();
   with
     e ->
     Sys.chdir "../";
     raise e
   end;
   Sys.chdir "../";
   ()


let build () =
  in_build_directory (fun () ->
      chain [
        "pwd";
        "cp ../pvem_lwt_file_lock.ml .";
        "ocamlfind ocamlc -package pvem,pvem_lwt_unix,unix,threads,lwt.preemptive,lwt.unix -thread -c pvem_lwt_file_lock.ml -o pvem_lwt_file_lock.cmo";
        "ocamlfind ocamlopt -package pvem,pvem_lwt_unix,unix,threads,lwt.preemptive,lwt.unix -thread -c pvem_lwt_file_lock.ml  -annot -bin-annot -o pvem_lwt_file_lock.cmx";
        "ocamlc pvem_lwt_file_lock.cmo -a -o pvem_lwt_file_lock.cma";
        "ocamlopt pvem_lwt_file_lock.cmx -a -o pvem_lwt_file_lock.cmxa";
        "ocamlopt pvem_lwt_file_lock.cmxa pvem_lwt_file_lock.a -shared -o pvem_lwt_file_lock.cmxs";
      ];
    )


let install () =
    in_build_directory (fun () ->
        chain [
          "ocamlfind install pvem_lwt_file_lock ../META pvem_lwt_file_lock.cmx pvem_lwt_file_lock.cmo pvem_lwt_file_lock.cma pvem_lwt_file_lock.cmi pvem_lwt_file_lock.cmxa pvem_lwt_file_lock.cmxs pvem_lwt_file_lock.a pvem_lwt_file_lock.o"
        ])


let merlinize () =
    chain [
      "echo 'S .' > .merlin";
      "echo 'B _build' >> .merlin";
      "echo 'PKG pvem' >> .merlin";
"echo 'PKG pvem_lwt_unix' >> .merlin";
"echo 'PKG unix' >> .merlin";
"echo 'PKG threads' >> .merlin";
"echo 'PKG lwt.preemptive' >> .merlin";
"echo 'PKG lwt.unix' >> .merlin";
     ]


let build_doc () =
    in_build_directory (fun () ->
        chain [
          "mkdir -p doc";
                       sprintf "ocamlfind ocamldoc -package pvem,pvem_lwt_unix,unix,threads,lwt.preemptive,lwt.unix -thread -charset UTF-8 -keep-code -colorize-code -html pvem_lwt_file_lock.ml -d doc/";
        ])


let name = "pvem_lwt_file_lock"

let () =
  match args with
  | _ :: "build" :: [] ->
    say "Building.";
    build ();
    say "Done."
  | _ :: "build_doc" :: [] ->
    say "Building the documentation.";
    build_doc ()
  | _ :: "install" :: [] ->
    say "Installing";
    install ();
  | _ :: "uninstall" :: [] ->
    chain [
      sprintf "ocamlfind remove %s" name
    ]
  | _ :: "merlinize" :: [] -> merlinize ()
  | _ :: "clean" :: []
  | _ :: "C" :: [] ->
    cmdf "rm -fr _build"
  | _ :: "help" :: [] ->
    say "usage: ocaml %s [build|build_doc|install|uninstall|clean|merlinize]" Sys.argv.(0);
  | _ ->
    say "usage: ocaml %s [build|build_doc|install|uninstall|clean|merlinize]" Sys.argv.(0);
    exit 1
    

