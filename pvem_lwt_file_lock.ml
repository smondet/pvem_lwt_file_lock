(**************************************************************************)
(*  Copyright (c) 2013,                                                   *)
(*                           Sebastien Mondet <seb@mondet.org>,           *)
(*                           Ashish Agarwal <agarwal1975@gmail.com>.      *)
(*                                                                        *)
(*  Permission to use, copy, modify, and/or distribute this software for  *)
(*  any purpose with or without fee is hereby granted, provided that the  *)
(*  above  copyright notice  and this  permission notice  appear  in all  *)
(*  copies.                                                               *)
(*                                                                        *)
(*  THE  SOFTWARE IS  PROVIDED  "AS  IS" AND  THE  AUTHOR DISCLAIMS  ALL  *)
(*  WARRANTIES  WITH  REGARD  TO  THIS SOFTWARE  INCLUDING  ALL  IMPLIED  *)
(*  WARRANTIES  OF MERCHANTABILITY AND  FITNESS. IN  NO EVENT  SHALL THE  *)
(*  AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL  *)
(*  DAMAGES OR ANY  DAMAGES WHATSOEVER RESULTING FROM LOSS  OF USE, DATA  *)
(*  OR PROFITS,  WHETHER IN AN  ACTION OF CONTRACT, NEGLIGENCE  OR OTHER  *)
(*  TORTIOUS ACTION,  ARISING OUT  OF OR IN  CONNECTION WITH THE  USE OR  *)
(*  PERFORMANCE OF THIS SOFTWARE.                                         *)
(**************************************************************************)

open Pvem_lwt_unix

module type FILE_LOCK = sig


  (** An attempt at NFS-compliant file-locking. *)


  val lock :
    string ->
    (bool,
     [> `lock of
          [> `path of string ] *
            [> `unix_link of exn | `write_file of exn ] ])
      Deferred_result.t
  (** Try to lock a file, return [true] if succeed.  *)

  val unlock :
    string ->
    (unit, [> `lock of [> `path of string ] * [> `unix_unlink of exn ] ])
      Deferred_result.t
  (** Unlock a file which is locked. *)

  val with_lock_gen : ?wait:float -> ?retry:int -> string ->
    f:(unit -> ('user_ok, 'user_error) Deferred_result.t) ->
    ([>
      | `ok of 'user_ok
      | `ok_but_not_unlocked of
          'user_ok * [> `lock of [> `path of string ] * [> `unix_unlink of exn ] ]
      | `error of 'user_error
      | `error_and_not_unlocked of
          'user_error * [> `lock of [> `path of string ] * [> `unix_unlink of exn ] ]
    ],
      [> `lock of
           [> `path of string ] *
             [> `system_sleep of exn
             | `too_many_retries of float * int
             | `unix_link of exn
             | `write_file of exn ] ])
      Deferred_result.t
  (** Run a function with a file locked. Retry [retry] times after
      waiting [wait] seconds between each attempt. The return value is
      as general as possible (on the [Ok _] side): {ul
        {li [`ok _] if everything is fine.}
        {li [`ok_but_not_unlocked _] if the function succeeded but
          unlocking did not.}
        {li [`error _] if the function failed but the locking and
          unlocking were fine.}
        {li [`error_and_not_unlocked _] if both the user function and the
          unlocking failed. }
      } *)

  val with_lock: ?wait:float -> ?retry:int -> string ->
    f:(unit ->
       ('a,
        [> `lock of
             [> `path of string ] *
               [> `system_sleep of exn
               | `too_many_retries of float * int
               | `unix_link of exn
               | `unix_unlink of exn
               | `write_file of exn ] ]
        as 'b)
         Deferred_result.t) ->
    ('a, 'b) Deferred_result.t
  (** Do like [with_lock_gen] but merge errors (locking errors take
      precedence over user-function results). *)

  val with_locks_gen :
    ?wait:float ->
    ?retry:int ->
    string list ->
    f:(unit -> ('user_ok, 'user_error) Deferred_result.t) ->
    ([> `error of 'user_error
     | `error_and_not_unlocked of
          'user_error * [> `multiple of [> `unlock of string * exn ] list ]
     | `ok of 'user_ok
     | `ok_but_not_unlocked of
          'user_ok * [> `multiple of [> `unlock of string * exn ] list ] ],
     [> `lock of
          [> `paths of string list ] *
            [> `multiple of
                 [> `lock of
                      string *
                        [> `unix_link of exn | `write_file of exn ]
                  | `unlock of string * exn ] list
            | `system_sleep of exn
            | `too_many_retries of float * int ] ])
      Deferred_result.t
  (** Do like [with_lock_gen] but with more than one files to lock. *)

  val with_locks :
    ?wait:float ->
    ?retry:int ->
    string list ->
    f:(unit ->
       ('a,
        [> `lock of
             [> `paths of string list ] *
               [> `multiple of
                    [> `lock of
                         string *
                           [> `unix_link of exn | `write_file of exn ]
                    |  `unlock of string * exn ] list
               | `system_sleep of exn
               | `too_many_retries of float * int ]
        | `multiple of [> `unlock of string * exn ] list ]
        as 'b)
         Deferred_result.t) ->
    ('a, 'b) Deferred_result.t
    (** Do like [with_locks_gen] but merge errors like in [with_lock]. *)

end

module File_lock : FILE_LOCK = struct

  open Deferred_result
  open Deferred_list
  module List = struct
    include ListLabels

    let map l ~f = List.rev (List.rev_map f l)

  end

  let lock_path path = path ^ ".lock"
  let lock_content path = path ^ ".empty"

  let wrap_unix path f =
    wrap_deferred (fun () -> f ()) ~on_exn:(fun e -> `exn e)

  let do_lock path =
    IO.write_file (lock_content path) ~content:"ABAB"
    >>= fun () ->
    wrap_unix path (fun () ->
        let open Lwt in
        catch (fun () ->
            Lwt_unix.link (lock_content path) (lock_path path)
            >>= fun () ->
            return true)
          (fun e -> return false))

  let lock path =
    do_lock path
    >>< begin function
    | `Ok b -> return b
    | `Error (`write_file_error (f, e)) ->
      fail (`lock (`path path, `write_file e))
    | `Error (`exn e) ->
      fail (`lock (`path path, `unix_link e))
    end

  let unlock path =
    wrap_unix path (fun () ->
        let open Lwt in
        Lwt_unix.unlink (lock_content path)
        >>= fun () ->
        Lwt_unix.unlink (lock_path path)
      )
    >>< begin function
    | `Ok () -> return ()
    | `Error (`exn e) -> fail (`lock (`path path, `unix_unlink e))
    end

  let with_lock_gen ?(wait=0.042) ?(retry=100) file ~f =
    let rec loop try_again =
      lock file
      >>= begin function
      | true ->
        f ()
        >>< begin function
        |`Ok res ->
          unlock file
          >>< begin function
          |`Ok () -> return (`ok res)
          |`Error e -> return (`ok_but_not_unlocked (res, e))
          end
        |`Error e1 ->
          unlock file
          >>< begin function
          |`Ok () -> return (`error e1)
          |`Error e2 -> return (`error_and_not_unlocked (e1, e2))
          end
        end
      | false when try_again > 0 ->
        System.sleep wait
        >>= fun () ->
        loop (try_again - 1)
      | false ->
        fail (`too_many_retries (wait, retry))
      end
    in
    loop retry
    >>< begin function
    |`Ok o -> return o
    |`Error (`lock m) -> fail (`lock m)
    |`Error (`system_exn e) -> fail (`lock (`path file, `system_sleep e))
    |`Error (`too_many_retries _ as e) -> fail (`lock (`path file, e))
    end

  let with_lock ?wait ?retry file ~f =
    with_lock_gen ?wait ?retry file ~f
    >>= begin function
    | `ok o -> return o
    | `error e -> fail e
    | `ok_but_not_unlocked (_, e) -> fail e
    | `error_and_not_unlocked (_, e2) -> fail (e2)
    end

  let with_locks_gen ?(wait=0.042) ?(retry=100) files ~f =
    let is_locked = function `locked _ -> true | _ -> false in
    let unlock_locked l =
      for_sequential l begin function
      | `locked f -> unlock f
      | `not_locked f -> return ()
      end
      >>= begin function
      | (oks, []) -> return ()
      | (oks, errs) ->
        let cleaner =
          List.map errs (function
            | `lock (`path p, `unix_unlink e) -> `unlock (p, e)) in
        fail (`multiple (cleaner : _ list))
      end
    in
    let rec loop try_again =
      for_sequential files (fun f ->
          lock f
          >>= begin function
          | true -> return (`locked f)
          | false -> return (`not_locked f)
          end)
      >>= begin function
      | (ok_list, []) when List.for_all ok_list ~f:is_locked ->
        f ()
        >>< begin function
        |`Ok res ->
          unlock_locked ok_list
          >>< begin function
          |`Ok () -> return (`ok res)
          |`Error e -> return (`ok_but_not_unlocked (res, e))
          end
        |`Error e ->
          unlock_locked ok_list
          >>< begin function
          |`Ok () -> return (`error e)
          |`Error e2 -> return (`error_and_not_unlocked (e, e2))
          end
        end
      | (ok_list, []) when try_again > 0 ->
        unlock_locked ok_list
        >>= fun () ->
        System.sleep wait >>= fun () ->
        loop (try_again - 1)
      | (ok_list, []) ->
        unlock_locked ok_list
        >>= fun () ->
        fail (`too_many_retries (wait, retry))
      | (ok_list, errs) ->
        unlock_locked ok_list
        >>= fun () ->
        let cleaner =
          List.map errs (function
            | `lock (`path p, e) -> `lock (p, e)) in
        fail (`multiple cleaner)
      end
    in
    loop retry
    >>< begin function
    |`Ok o -> return o
    |`Error (`lock m) -> fail (`lock m)
    |`Error (`multiple m) -> fail (`lock (`paths files, `multiple m))
    |`Error (`system_exn m) -> fail (`lock (`paths files, `system_sleep m))
    |`Error (`too_many_retries m) -> fail (`lock (`paths files, `too_many_retries m))
    end

  let with_locks ?wait ?retry file ~f =
    with_locks_gen ?wait ?retry file ~f
    >>= begin function
    | `ok o -> return o
    | `error e -> fail e
    | `ok_but_not_unlocked (_, e) -> fail e
    | `error_and_not_unlocked (_, e2) -> fail (e2)
    end

end
